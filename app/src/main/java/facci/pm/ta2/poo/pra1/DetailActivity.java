package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {
ImageView imagenJavier;
TextView nombreJavier, precioJavier, descripcionJavier;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
       imagenJavier = findViewById(R.id.thumbnail);
       nombreJavier = findViewById(R.id.nombre);
       precioJavier = findViewById(R.id.precio);
       descripcionJavier = findViewById(R.id.descripcion);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");


        // INICIO - CODE6
     //Recibimos el parametro de la anterior activity
     String object_id = getIntent().getStringExtra("object_id");
     //crea la clase DataQuery se asigna un objeto varibale (item)
     DataQuery query = DataQuery.get("item");
     //se crea el metodo getInBackground mediante esto premite administrar o ingresar datos
     query.getInBackground(object_id, new GetCallback<DataObject>() {
         @Override
         public void done(DataObject object, DataException e) {
             if (e==null){
                 // a todos se les pone String tipo cadena
                 String nombre = (String)  object.get("name");
                 String precio = (String) object.get("price");
                 Bitmap bitmap = (Bitmap)  object.get("image");
                 String descripcion = (String) object.get("description");
                 nombreJavier.setText(nombre+"$");
                 precioJavier.setText(precio);
                 descripcionJavier.setText(descripcion);
                 imagenJavier.setImageBitmap(bitmap);
             }
         }
     });

        // FIN - CODE6

    }

}
